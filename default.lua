-- ----------------------------------------
-- Check if we're in EditMode by getting the class of the current screen.
-- We could check the current screen's name, but it's possible for a theme
-- to rename screens as it sees fit.

local IsEditMode = function()
	local screen = SCREENMAN:GetTopScreen()
	if not screen then
		SCREENMAN:SystemMessage("IsEditMode() check failed to run because there is no Screen yet.")
		return nil
	end

	return (THEME:GetMetric(screen:GetName(), "Class") == "ScreenEdit")
end

local BothPlayersAreOnBeginner = function()
	for _, player in ipairs(GAMESTATE:GetHumanPlayers()) do
		if GAMESTATE:GetCurrentSteps(player):GetDifficulty() ~= "Difficulty_Beginner" then
			return false
		end
	end
	return true
end

-- if both players are joined
if #GAMESTATE:GetHumanPlayers() > 1 then

	-- if both players are joined and both are on beginner
	-- just return an empty Actor now and don't proceed any futher into this Lua
	if BothPlayersAreOnBeginner() then return Def.Actor{} end

	-- otherwise, both players are joined but on different difficulties,
	-- so we need to set both to beginner and reload
	local darkness = GAMESTATE:GetCurrentSong()
	local fallen_interlude = darkness:GetOneSteps("StepsType_Dance_Single", "Difficulty_Beginner")

	-- set steps for both players to beginner
	for _, player in ipairs(GAMESTATE:GetHumanPlayers()) do
		GAMESTATE:SetCurrentSteps(player, fallen_interlude)
	end

	-- and return an Actor that will reload the screen as soon as it is ready
	return Def.Actor{
		OnCommand=function(self)
			if not IsEditMode() then SCREENMAN:SetNewScreen('ScreenGameplay') end
		end
	}
end

-- if only one player is joined and is on the beginner chart, just return an empty Actor
if #GAMESTATE:GetHumanPlayers() == 1
and GAMESTATE:GetCurrentSteps(GAMESTATE:GetMasterPlayerNumber()):GetDifficulty() == "Difficulty_Beginner" then
	return Def.Actor{
		OnCommand=function(self) self:sleep(130):queuecommand("EndGameplay") end,
		EndGameplayCommand=function(self) SCREENMAN:GetTopScreen():StartTransitioningScreen("SM_DoNextScreen") end
	}
end



-- ----------------------------------------
-- Distant Towers
-- ----------------------------------------

local asset_width = WideScale(1440,1920)

local scenes = {
	{start=9,  finish=31},
	{start=35.333, finish=72},
	{start=90.667, finish=105},
	{start=106.667, finish=150},
	{start=154, finish=211}
}

local fade_duration = 1.666

-- ----------------------------------------
-- Actors

local af =  Def.ActorFrame{}

af.OnCommand=function(self)
	if not IsEditMode() then
		for scene in ivalues(scenes) do
			scene.af:hibernate(scene.start)
				:queuecommand("Show")
				:sleep( math.max(scene.finish - scene.start - fade_duration, 0) )
				:queuecommand( "FadeOut" )
		end
	end
end

-- fade out Gameplay's UI and then call visible(false) on just about everything
af[#af+1] = Def.Quad{
	InitCommand=function(self) self:FullScreen():Center():diffuse(0,0,0,0) end,
	OnCommand=function(self)
		if not IsEditMode() then
			self:smooth(1):diffusealpha(1):queuecommand("HideEverything")
		end
	end,
	HideEverythingCommand=function(self)
		local screen = SCREENMAN:GetTopScreen()
		for k,v in pairs(screen:GetChildren()) do
			if not (k == "SongForeground" or k == "Debug") then
				v:visible(false)
			end
		end
	end
}

-- audio
af[#af+1] = LoadActor("./Distant Towers.ogg")..{
	OnCommand=function(self)
		if not IsEditMode() then self:play() end
	end
}

-- scenes
for i=1, #scenes do
	af[#af+1] = LoadActor("./assets/"..i.."/default.lua")..{
		InitCommand=function(self)
			scenes[i].af = self
			self:Center()
			self:zoom(_screen.w/asset_width)
		end,
		FadeOutCommand=function(self) self:smooth(fade_duration):diffusealpha(0):queuecommand("Hibernate") end,
		HibernateCommand=function(self) self:hibernate(math.huge) end
	}
end

return af