local af = Def.ActorFrame{}

-- dark
af[#af+1] = LoadActor("./scene-dark.jpg")..{
	InitCommand=function(self) self:diffuse(0,0,0,0) end,
	ShowCommand=function(self) self:smooth(3):diffuse(1,1,1,1) end,
}

-- illuminated
af[#af+1] = LoadActor("./scene-lit.jpg")..{
	InitCommand=function(self) self:diffusealpha(0) end,
	ShowCommand=function(self) self:sleep(3):accelerate(1.333):diffusealpha(1) end,
}


-- descending moon
af[#af+1] = LoadActor("./moon.png")..{
	InitCommand=function(self) self:valign(1):y(-350):zoom(0.8):diffusealpha(0) end,
	ShowCommand=function(self) self:sleep(3):linear(18):diffusealpha(1):y(-240) end
}

-- trees
af[#af+1] = LoadActor("./trees1.png")..{
	InitCommand=function(self) self:diffuse(0,0,0,0) end,
	ShowCommand=function(self) self:sleep(1):smooth(1.666):diffuse(1,1,1,0.5) end,
}
af[#af+1] = LoadActor("./trees2.png")..{
	InitCommand=function(self) self:diffuse(0,0,0,0) end,
	ShowCommand=function(self) self:sleep(1):smooth(1.666):diffuse(1,1,1,0.5) end,
}


for i=1,3 do
	af[#af+1] = LoadActor("./stars" .. i .. ".png")..{
		InitCommand=function(self) self:diffusealpha(0) end,
		ShowCommand=function(self)
			self:sleep(3 + (0.5*(i-1))):decelerate(1):diffusealpha(1)
				:diffuseshift():effectperiod(3+i):effectoffset(3-i):effectcolor1(1,1,1,1):effectcolor2(1,1,1,0)
		end,
	}
end

for i=1,3 do
	af[#af+1] = LoadActor("./stars" .. i .. ".png")..{
		InitCommand=function(self) self:diffusealpha(0) end,
		ShowCommand=function(self)
			self:sleep(4 + (0.5*(i-1)))
				:decelerate(1):diffusealpha(0.5)
				:pulse():effectperiod(4+i):effectoffset(3-i):effectmagnitude(1.025,1,1)
		end
	}
end


for i=1,3 do
	af[#af+1] = LoadActor("./person" .. i .. ".png")..{
		InitCommand=function(self) self:diffusealpha(0) end,
		ShowCommand=function(self) self:sleep(4 + (6*(i-1))):smooth(1):diffusealpha(1):sleep(4):smooth(1):diffusealpha(0) end,
	}
end

return af